module subs
use mtmod
implicit none
integer, parameter :: dp = kind(0.0d0)
contains

subroutine BuildBxx(gh, n, A)
    ! Build Bxx according to "Spin Polarized Non-Relativistic Fermions in 1+1 Dimensions", p. 3:
    ! Coupling $gh = \hat{\gamma}$, spatial lattice size n.
    implicit none
    real(dp), dimension(:,:) :: A ! add "allocatable" if A is not allocated in main program
    real(dp) :: gh
    integer :: i, j, n

    ! allocate(A(n,n)) ! allocate Bxx in main program!

    do j = 1, n
        do i = 1, n
            A(i,j) = 0
        end do
    enddo
    do i = 1, n
        A(i,i) = 1 + gh
    enddo
    do i = 1, n-1
        A(i,i+1) = -gh/2
        A(i+1,i) = -gh/2
    enddo
    A(1,n) = -gh/2
    A(n,1) = -gh/2    
end subroutine BuildBxx

subroutine BuildBxxOpen(gh, n, A)
    ! Build Bxx according to "Spin Polarized Non-Relativistic Fermions in 1+1 Dimensions", p. 3:
    ! Coupling $gh = \hat{\gamma}$, spatial lattice size n.
    implicit none
    real(dp), dimension(:,:) :: A ! add "allocatable" if A is not allocated in main program
    real(dp) :: gh
    integer :: i, j, n

    ! allocate(A(n,n)) ! allocate Bxx in main program!

    do j = 1, n
        do i = 1, n
            A(i,j) = 0
        end do
    enddo
    do i = 1, n
        A(i,i) = 1 + gh
    enddo
    do i = 1, n-1
        A(i,i+1) = -gh/2
        A(i+1,i) = -gh/2
    enddo
    ! No elements in corners for open boundary conditions. 
end subroutine BuildBxxOpen

subroutine BuildCxx(A, n, mat)
    ! Build Cxx according to "Spin Polarized Non-Relativistic Fermions in 1+1 Dimensions", p. 3:
    ! $A = A_t$ indicates the slice of the lattice fiels A at time t, spatial lattice size n = size(A).
    implicit none
    real(dp), dimension(n), intent(in) :: A
    real(dp), dimension(:,:), allocatable :: mat
    integer, intent(in) :: n
    integer :: i, j

    if (n /= size(A)) stop "Size mismatch between A_t and nx."

    allocate(mat(n,n))
    
    do j = 1, n
        do i = 1, n
            mat(i,j) = 0
        end do
    enddo
    do i=1,n
        mat(i,i) = exp(A(i))
    enddo
end subroutine BuildCxx

subroutine IdentityMatrix(mat, n)
    ! Creates a n x n identity matrix.
    implicit none
    real(dp), dimension(:,:), allocatable, intent(inout) :: mat
    integer, intent(in) :: n
    integer :: i, j

    allocate(mat(n,n))

    do j = 1, n
        do i = 1, n
            mat(i,j) = 0
        end do
    enddo
    do i=1,n
        mat(i,i) = 1
    enddo    
end subroutine IdentityMatrix

subroutine FindDet(mat, det)
    implicit none
    real(dp), intent(in) :: mat(:,:)
    real(dp), intent(inout) :: det
    real(dp), dimension(:,:), allocatable :: A
    integer, allocatable :: ipiv(:)
    integer :: i, n, info
    real :: sgn

    n = size(mat, 1)
    allocate(A(n,n), ipiv(n))

    A = mat
    call dgetrf(n, n, A, n, ipiv, info)
    if (info < 0) stop "lufact: info < 0, illegal argument"

    det = 1
    do i = 1, n
        det = det * A(i,i)              
    enddo

    sgn = 1
    do i = 1, n
        if(ipiv(i) /= i) then
            sgn = -sgn
        endif        
    enddo
    
    det = sgn * det
end subroutine FindDet

subroutine InvertMat(mat, inv)
    ! Invert the square matrix mat using dgetrf and dgetri from LAPACK.
    implicit none
    real(dp), intent(in) :: mat(:,:)
    real(dp), dimension(:,:), allocatable, intent(inout) :: inv
    integer :: n, info
    real(dp), allocatable :: work(:)
    integer, allocatable :: ipiv(:)

    n = size(mat, 1)

    allocate(inv(n,n), work(n), ipiv(n))

    inv = mat
    call dgetrf(n, n, inv, n, ipiv, info)
    if (info /= 0) stop "Matrix is numerically singular."
    call dgetri(n, inv, n, ipiv, work, n, info)
    if (info /= 0) stop "Matrix inversion failed."    
end subroutine InvertMat

subroutine MatPower(mat, power, A)
    implicit none
    integer, intent(in) :: power
    real(dp), intent(in) :: mat(:,:)
    real(dp), dimension(:,:) :: A ! Allocate A in main program.
    integer :: i, n

    ! n = size(mat, 1)
    ! allocate(A(n,n))

    A = mat
    do i = 1, power - 1
        A = matmul(A, mat)        
    enddo    
end subroutine MatPower

subroutine Minor(A, B, mat, min)
    ! Calculate the minor one gets by removing the rows A and the columns B.
    implicit none
    integer, intent(in) :: A(:), B(:)
    real(dp), intent(in) :: mat(:,:)
    real(dp), intent(out) :: min
    real(dp), dimension(:,:), allocatable :: minormat
    integer :: n, i, j, ind, indrow, indcol
    integer, dimension(:), allocatable :: Akept, Bkept

    if (size(A) /= size(B)) stop "The index sets A and B must be of the same length."

    n = size(mat, 1) - size(A)

    if (n == 0) then
        min = 1 ! The minor where all rows and columns are removed is defined as 1.
    else
        allocate(minormat(n,n), Akept(n), Bkept(n)) ! The determinant of this matrix will be the minor.

        ! Create Akept and Bkept, these are sorted.
        ind = 1
        do i=1,size(mat,1)
            if (any(A==i) .eqv. .false.) then
                Akept(ind) = i
                ind = ind + 1
            endif        
        enddo
        ind = 1
        do i=1,size(mat,1)
            if (any(B==i) .eqv. .false.) then
                Bkept(ind) = i
                ind = ind + 1
            endif        
        enddo

        ! Create minormat
        indrow = 1
        indcol = 1
        do j=1,n
            do i=1,n
                minormat(i,j) = mat(Akept(i),Bkept(j))            
            enddo        
        enddo

        call FindDet(minormat, min)
    endif
end subroutine Minor

subroutine ComplementaryMinor(A, B, mat, cmin)
    ! Calculate the complementary minor one gets by keeping the rows A and the columns B.
    implicit none
    integer, intent(in) :: A(:), B(:)
    real(dp), intent(in) :: mat(:,:)
    real(dp), intent(out) :: cmin
    real(dp), dimension(:,:), allocatable :: cminormat
    integer :: n, i, j, indrow, indcol, inttemp
    integer, dimension(:), allocatable :: Asort, Bsort

    if (size(A) /= size(B)) stop "The index sets A and B must be of the same length."

    n = size(A)

    if (n == 0) then
        cmin = 1 ! The complementary minor where no rows and columns are left is defined as 1.
    else
        allocate(cminormat(n,n), Asort(n), Bsort(n)) ! The determinant of this matrix will be the complemetary minor.

        ! Sort A and B
        Asort = A
        Bsort = B
        ! ! Comment this block if A and B are already sorted.
        ! do i=1,n-1
        !     do j=i+1,n
        !         if (Asort(i)>Asort(j)) then
        !             inttemp = Asort(i)
        !             Asort(i) = Asort(j)
        !             Asort(j) = inttemp
        !         endif                
        !     enddo
        ! enddo
        ! do i=1,n-1
        !     do j=i+1,n
        !         if (Bsort(i)>Bsort(j)) then
        !             inttemp = Bsort(i)
        !             Bsort(i) = Bsort(j)
        !             Bsort(j) = inttemp
        !         endif                
        !     enddo
        ! enddo

        ! Create cminormat
        indrow = 1
        indcol = 1
        do j=1,n
            do i=1,n
                cminormat(i,j) = mat(Asort(i),Bsort(j))            
            enddo        
        enddo

        call FindDet(cminormat, cmin)
    endif
end subroutine ComplementaryMinor

subroutine Cofactor(A, B, mat, cof)
    ! Calculate the cofactor one gets by removing the rows A and the columns B.
    implicit none
    integer, intent(in) :: A(:), B(:)
    real(dp), intent(in) :: mat(:,:)
    real(dp), intent(out) :: cof
    real(dp), dimension(:,:), allocatable :: cofactormat
    integer :: n, i, j, ind, indrow, indcol
    integer, dimension(:), allocatable :: Akept, Bkept

    if (size(A) /= size(B)) stop "The index sets A and B must be of the same length."

    n = size(mat, 1) - size(A)

    if (n==0) then
        cof = 1 ! Define the complementary cofactor where no rows and columns are removed as $(-1)^{Mod[Sum(A)+Sum(B), 2]} = 1 \ for \ A = B$.
    else
        allocate(cofactormat(n,n), Akept(n), Bkept(n)) ! The determinant of this matrix times $(-1)^{Mod[Sum(A)+Sum(B), 2]$ will be the cofactor.

        ! Create Akept and Bkept, these are sorted.
        ind = 1
        do i=1,size(mat,1)
            if (any(A==i) .eqv. .false.) then
                Akept(ind) = i
                ind = ind + 1
            endif        
        enddo
        ind = 1
        do i=1,size(mat,1)
            if (any(B==i) .eqv. .false.) then
                Bkept(ind) = i
                ind = ind + 1
            endif        
        enddo

        ! Create cofactormat
        indrow = 1
        indcol = 1
        do j=1,n
            do i=1,n
                cofactormat(i,j) = mat(Akept(i),Bkept(j))            
            enddo        
        enddo

        call FindDet(cofactormat, cof)
        cof = (-1)**(modulo(sum(A)+sum(B),2))*cof ! This is the only difference to the minor.
    endif
end subroutine Cofactor

subroutine ComplementaryCofactor(A, B, mat, ccof)
    ! Calculate the complementary cofactor one gets by keeping the rows A and the columns B.
    implicit none
    integer, intent(in) :: A(:), B(:)
    real(dp), intent(in) :: mat(:,:)
    real(dp), intent(out) :: ccof
    real(dp), dimension(:,:), allocatable :: ccofmat
    integer :: n, i, j, indrow, indcol, inttemp
    integer, dimension(:), allocatable :: Asort, Bsort

    if (size(A) /= size(B)) stop "The index sets A and B must be of the same length."

    n = size(A)

    if (n==0) then
        ccof = 1 ! Define the complementary cofactor where no rows and columns are removed as $(-1)^{Mod[Sum(A)+Sum(B), 2]} = 1 \ for \ A = B$.
    else
        allocate(ccofmat(n,n)) ! The determinant of this matrix times $(-1)^{Mod[Sum(A)+Sum(B), 2]$ will be the complementary cofactor.

        ! Sort A and B
            Asort = A
            Bsort = B
            do i=1,n-1
                do j=i+1,n
                    if (Asort(i)>Asort(j)) then
                        inttemp = Asort(i)
                        Asort(i) = Asort(j)
                        Asort(j) = inttemp
                    endif                
                enddo
            enddo
            do i=1,n-1
                do j=i+1,n
                    if (Bsort(i)>Bsort(j)) then
                        inttemp = Bsort(i)
                        Bsort(i) = Bsort(j)
                        Bsort(j) = inttemp
                    endif                
                enddo
            enddo

        ! Create ccofmat
        indrow = 1
        indcol = 1
        do j=1,n
            do i=1,n
                ccofmat(i,j) = mat(Asort(i),Bsort(j))            
            enddo        
        enddo

        call FindDet(ccofmat, ccof)
        ccof = (-1)**(modulo(sum(A)+sum(B),2))*ccof ! This is the only difference to the complementary minor.
    endif
end subroutine ComplementaryCofactor

subroutine MinorMatrix(mat, n, minormat)
    ! Calculate the matrix containing all minors of order n, i.e. where n rows and columns are removed. 
    ! Due to restrictions in the calculation of the binomial coefficient, only matrices up to size 33 x 33 are supported.
    implicit none
    real(dp), intent(in) :: mat(:,:)
    integer, intent(in) :: n
    real(dp), dimension(:,:), allocatable :: minormat
    integer, dimension(:,:), allocatable :: indices
    integer :: m, i, j

    if(size(mat,1)>33) stop "Can't create minor matrix with size bigger than Binomial(33, 16)"

    m = Binomial(size(mat,1), n)

    allocate(minormat(m,m))

    call IndexSet(n, size(mat,1), indices) ! indices now holds all sorted index sets, index(i,:) holds the i-th such set.

    do j=1,m
        do i=1,m
            call Minor(indices(i,:), indices(j,:), mat, minormat(i,j))
        enddo        
    enddo    
end subroutine MinorMatrix

subroutine ComplementaryMinorMatrix(mat, n, cminormat)
    ! Calculate the matrix containing all complementary minors of order n, i.e. where n rows and columns are kept. 
    ! Due to restrictions in the calculation of the binomial coefficient, only matrices up to size 33 x 33 are supported.
    implicit none
    real(dp), intent(in) :: mat(:,:)
    integer, intent(in) :: n
    real(dp), dimension(:,:), allocatable :: cminormat
    integer, dimension(:,:), allocatable :: indices
    integer :: m, i, j

    if(size(mat,1)>33) stop "Can't create complementary minor matrix with size bigger than Binomial(33, 16)"

    m = Binomial(size(mat,1), n)

    allocate(cminormat(m,m))

    call IndexSet(n, size(mat,1), indices) ! indices now holds all sorted index sets, index(i,:) holds the i-th such set.

    do j=1,m
        do i=1,m
            call ComplementaryMinor(indices(i,:), indices(j,:), mat, cminormat(i,j))
        enddo        
    enddo    
end subroutine ComplementaryMinorMatrix

subroutine CofactorMatrix(mat, n, cofactormat)
    ! Calculate the matrix containing all cofactors of order n, i.e. where n rows and columns are removed. 
    ! Due to restrictions in the calculation of the binomial coefficient, only matrices up to size 33 x 33 are supported.
    implicit none
    real(dp), intent(in) :: mat(:,:)
    integer, intent(in) :: n
    real(dp), dimension(:,:), allocatable :: cofactormat
    integer, dimension(:,:), allocatable :: indices
    integer :: m, i, j

    if(size(mat,1)>33) stop "Can't create cofactor matrix with size bigger than Binomial(33, 16)"

    m = Binomial(size(mat,1), n)

    allocate(cofactormat(m,m))

    call IndexSet(n, size(mat,1), indices) ! indices now holds all sorted index sets, index(i,:) holds the i-th such set.

    do j=1,m
        do i=1,m
            call Cofactor(indices(i,:), indices(j,:), mat, cofactormat(i,j))
        enddo        
    enddo    
end subroutine CofactorMatrix

subroutine ComplementaryCofactorMatrix(mat, n, ccofactormat)
    ! Calculate the matrix containing all complementary cofactors of order n, i.e. where n rows and columns are kept. 
    ! Due to restrictions in the calculation of the binomial coefficient, only matrices up to size 33 x 33 are supported.
    implicit none
    real(dp), intent(in) :: mat(:,:)
    integer, intent(in) :: n
    real(dp), dimension(:,:), allocatable :: ccofactormat
    integer, dimension(:,:), allocatable :: indices
    integer :: m, i, j

    if(size(mat,1)>33) stop "Can't create complementary cofactor matrix with size bigger than Binomial(33, 16)"

    m = Binomial(size(mat,1), n)

    allocate(ccofactormat(m,m))

    call IndexSet(n, size(mat,1), indices) ! indices now holds all sorted index sets, index(i,:) holds the i-th such set.

    do j=1,m
        do i=1,m
            call ComplementaryCofactor(indices(i,:), indices(j,:), mat, ccofactormat(i,j))
        enddo        
    enddo    
end subroutine ComplementaryCofactorMatrix

subroutine IndexSet(m_max, n_max, indices)
    ! Create all possible index sets of length m_max from {1,...,n_max}.
    implicit none
    integer, intent(in) :: m_max, n_max
    integer, dimension(:,:), allocatable :: indices
    integer, dimension(m_max) :: comb
    integer :: i, j, n

    n = Binomial(n_max, m_max)
    allocate(indices(n,m_max)) ! indices will hold all sorted index sets, index(i,:) holds the i-th such set.

    do j=1,m_max
        do i=1,n
           indices(i,j) = 0
        enddo        
    enddo

    call gen(1,m_max,n_max,indices, comb)
end subroutine IndexSet

recursive subroutine gen(m, m_max, n_max, indices, comb)
    ! Recursive subroutine needed to generate all possible indexsets. indices holds these indexsets.
    implicit none
    integer, intent(in) :: m, m_max, n_max
    integer :: indices(:,:)
    integer, dimension(m_max) :: comb
    integer :: n

    if (m > m_max) then
        do n=1, size(indices,1)
            if (indices(n,1) == 0) then
                indices(n,:) = comb
                exit
            endif                      
        enddo
    else
        do n=1, n_max
            if ((m == 1) .or. (n > comb(m-1))) then
                comb(m) = n
                call gen(m+1, m_max, n_max, indices, comb)
            endif            
        enddo
    endif    
end subroutine gen

subroutine ComplIndex(A, Akept, n)
    ! Calculate the complemetary index set of A.
    implicit none
    integer, intent(in) :: A(:),  n
    integer :: m, i, ind
    integer, dimension(:), allocatable :: Akept

    m = n - size(A)
    allocate(Akept(m))

    ! Create Akept.
    ind = 1
    do i=1,n
        if (any(A==i) .eqv. .false.) then
            Akept(ind) = i
            ind = ind + 1
        endif        
    enddo
    
end subroutine ComplIndex

subroutine FisherYates(a, n)
    ! Stores a random permutation of {1,...,n} of length m = size(a) in a.
    implicit none
    integer, intent(inout) :: a(:)
    integer, intent(in) :: n
    integer :: i, m, temp, rand
    integer, dimension(n) :: b
    real(dp) :: u

    m = size(a)
    do i=1,n
        b(i) = i
    enddo

    ! Shuffle
    do i=1,n-1
        u = grnd()
        rand = i + floor((n + 1 - i)*u)
        temp = b(rand)
        b(rand) = b(i)
        b(i) = temp        
    enddo
    a = b(1:m)
end subroutine FisherYates

subroutine FisherYatesSort(a, n)
    ! Stores a random permutation of {1,...,n} of length m = size(a) in a and sorts it in ascending order.
    implicit none
    integer, intent(inout) :: a(:)
    integer, intent(in) :: n
    integer :: i, j, m, temp

    call FisherYates(a, n)
    m = size(a)
    do i=1,m-1
        do j=i+1,m
            if (a(i)>a(j)) then
                temp = a(i)
                a(i) = a(j)
                a(j) = temp
            endif                
        enddo
    enddo
end subroutine FisherYatesSort

subroutine RandomSwitch(A, Nx)
    ! Puts a random fermion in A on another random free position.
    implicit none
    integer, intent(inout) :: A(:)
    integer, intent(in) :: Nx
    integer, dimension(:), allocatable :: Akept
    integer :: index1, index2, i, j, m, temp
    real(dp) :: u

    call ComplIndex(A, Akept, Nx)

    u = grnd()
    index1 = 1 + floor(size(A)*u) ! Generate random integer in {1, size(A)}.
    u = grnd()
    index2 = 1+ floor(size(Akept)*u)

    A(index1) = Akept(index2) ! Make the random switch.

    ! Sort A in ascending order.
    m = size(A)
    do i=1,m-1
        do j=i+1,m
            if (A(i)>A(j)) then
                temp = A(i)
                A(i) = A(j)
                A(j) = temp
            endif                
        enddo
    enddo    
end subroutine RandomSwitch

subroutine RandomMove(A, Nx)
    ! Puts a random fermion in A on the next free position to the right or left.
    implicit none
    integer, intent(inout) :: A(:)
    integer, intent(in) :: Nx
    integer, dimension(:), allocatable :: Akept
    integer :: index, m, i, j, temp
    real(dp) :: u

    call ComplIndex(A, Akept, Nx)

    u = grnd()
    index = 1 + floor(size(A)*u) ! Generate random integer in {1, size(A)}.

    u = grnd() ! Determines if the fermion is moved to the left or right.

    if (u <= 0.5) then ! Move to the right.
        ! Move A(index) to the next free position to the right.
        if (A(index) < Akept(size(Akept))) then
            A(index) = minval(Akept, mask=(Akept > A(index))) ! Assign the smallest entry in Akept bigger than A(index) to Aindex
        else
            A(index) = Akept(1) ! If A(index) is larger than the last element in Akept, then assign the first element of Akept to A(index).
        endif
    else ! Move to the left.
        ! Move A(index) to the next free position to the left.
        if (A(index) > Akept(1)) then
            A(index) = maxval(Akept, mask=(Akept < A(index))) ! Assign the largest entry in Akept smaller than A(index) to Aindex
        else
            A(index) = Akept(size(Akept)) ! If A(index) is smaller than the first element in Akept, then assign the last element of Akept to A(index).
        endif
    endif

    ! Sort A in ascending order.
    m = size(A)
    do i=1,m-1
        do j=i+1,m
            if (A(i)>A(j)) then
                temp = A(i)
                A(i) = A(j)
                A(j) = temp
            endif                
        enddo
    enddo    
end subroutine RandomMove

subroutine MoveAll(A, Nx)
    ! Move every fermion one position to the right.
    implicit none
    integer, intent(inout) :: A(:)
    integer, intent(in) :: Nx
    integer :: m, i, j, temp

    m = size(A)

    do i=1,m
        A(i) = modulo(A(i),Nx)+1
    enddo

    ! Sort A in ascending order.
    do i=1,m-1
        do j=i+1,m
            if (A(i)>A(j)) then
                temp = A(i)
                A(i) = A(j)
                A(j) = temp
            endif                
        enddo
    enddo    
end subroutine MoveAll

subroutine AddRandomIndex(A, B, Nx)
    ! Copies the indices from A to B and adds a random free index. size(B) = size(A) + 1
    implicit none
    integer, intent(inout) :: A(:), B(:)
    integer, intent(in) :: Nx
    integer, dimension(:), allocatable :: Acompl
    integer :: index, i, j, temp
    real(dp) :: u

    call ComplIndex(A, Acompl, Nx)

    u = grnd()
    index = 1 + floor(size(Acompl)*u) ! Generate random integer in {1, size(Acompl)}.

    do i=1, size(A)
        B(i) = A(i) ! copy A to B
    enddo
    B(size(B)) = Acompl(index) ! Add random free index to B.

    ! Sort B in ascending order.
    do i=1,size(B)-1
        do j=i+1,size(B)
            if (B(i)>B(j)) then
                temp = B(i)
                B(i) = B(j)
                B(j) = temp
            endif                
        enddo
    enddo    
end subroutine AddRandomIndex

subroutine RemoveRandomIndex(A, B, Nx)
    ! Copies the indices from B to A and removes a random index. size(B) = size(A) + 1
    implicit none
    integer, intent(inout) :: A(:), B(:)
    integer, intent(in) :: Nx
    integer :: index, i
    real(dp) :: u

    u = grnd()
    index = 1 + floor(size(B)*u) ! Generate random integer in {1, size(B)}.

    if (index == size(B)) then
        do i=1, index-1
            A(i) = B(i)
        enddo
    elseif (index == 1) then
        do i=2, size(B)
            A(i-1) = B(i)
        enddo
    else
        do i=1, index-1
            A(i) = B(i)
        enddo
        do i=index+1, size(B)
            A(i-1) = B(i)
        enddo
    endif
end subroutine RemoveRandomIndex

subroutine OccupationNumber(a, b, nx, occ)
    ! Gives the occupation number for each point for two given index sets.
    implicit none
    integer, dimension(:), intent(in) :: a, b
    integer, dimension(:), intent(inout) :: occ
    integer, intent(in) :: nx
    integer, dimension(:), allocatable :: abin, bbin
    integer :: i

    allocate(abin(nx), bbin(nx))

    ! Initialize abin and bbin with zeros
    do i=1,nx
        abin(i) = 0
        bbin(i) = 0
    enddo

    ! Mark where a and b are occupied
    do i=1, size(a)
        abin(a(i)) = 1
    enddo
    do i=1, size(b)
        bbin(b(i)) = 1
    enddo

    ! Generate occ
    do i=1, nx
        occ(i) = abin(i) + bbin(i)
    enddo
    
end subroutine OccupationNumber

function Binomial(n, k) result(coeff)
    ! Gives the binomial coefficient (n k) up to (n k) = (33 17).
    implicit none
    integer :: n, k, coeff
    if (n > 33) stop "Binomial(n, k): n > 33 is too large for this function."
    coeff = nint(gamma(dble(n+1))/(gamma(dble(k+1))*gamma(dble(n-k+1))))        
end function Binomial

end module subs