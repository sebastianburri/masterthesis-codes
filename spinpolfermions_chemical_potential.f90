program spinpolfermions
    ! Run ./spinpolfermions gamma index outputname seed_modifier in terminal, e.g. ./spinpolfermions 0.025 13 test.dat 221412.
    ! The index determines which gh is used: gh = index/100, it must be an integer between 1 and 1000.
    ! All other variables are set here before execution of the program.
    use subs
    implicit none
    integer :: i, j, k, l, m, n, Nx, Nt, norder1, norder2, Nstat, Nmeasure, nblocks, seedmod
    integer :: acceptance, index, sector ! Determines in which sector the simulation runs, 1 for sector n, 0 for n+1.
    integer(dp) :: sector1, sector2 ! sector1 counts how many simulations were in sector n, sector2 how many in sector n+1.
    integer, dimension(:), allocatable :: A11, A12
    integer, dimension(:,:), allocatable :: Arold1, Arnew1, Arold2, Arnew2, occold, occnew, occup ! occ will be used to store the occupation numbers.
    integer, dimension(:,:), allocatable :: Brold1, Brnew1, Brold2, Brnew2 ! Index sets used for the n+1 sector.
    real(dp), dimension(:,:), allocatable :: Bxx, PBxx
    real(dp), dimension(:), allocatable :: Woldarr1, Wnewarr1, Woldarr2, Wnewarr2, Wuparr1, Wuparr2 ! The variables with "up" will be nedded to switch sector.
    real(dp) :: Wold1, Wnew1, Wold2, Wnew2, Wold, Wnew, gh, gamma, simweight ! In simweight just the changes in weights and not the whole weight is considered.
    real(dp), dimension(1000,5) :: weights ! Used to store the f_alphas, also contains indices and gh:
                                           ! weights(index,:) = index, gh, f0, f1, f2
    character(len=32) :: indexarg, outputname, seedmodarg, gammaarg ! Used to pass arguments on execution via getarg.
    real(dp) :: f0, f1, f2, u ! These will be f_alpha, alpha = 0, 1, 2. u for double precision mersenne twister RNG.
    real :: start_time, stop_time

    ! Load the f_alphas into weights.
    open(12, file="falpha.dat")
    read(12,*) weights
    close(12)

    ! Read seed modifier from from invocation of program.
    call getarg(4, seedmodarg)
    read(seedmodarg,*) seedmod
    ! Initialise random generator
    call sgrnd(defaultsd + seedmod)

    Nx = 4
    Nt = 4
    nblocks = Nt ! Blocks for multiplication, must divide Nt without rest. Must be equal to Nt for the interacting system.
    norder1 = 2 ! Number of fermions of type 1. This scheme is used for all relevant objects.
    norder2 = 2 ! Number of fermions of type 2.
    if (modulo(Nt, nblocks) /= 0) stop "This number of blocks is not possible for the given Nt."
    Nstat = 100000 ! Number of steps for one measurement
    Nmeasure = 1000 ! Number of measurements

    allocate(A11(norder1), A12(norder2), Bxx(Nx, Nx), PBxx(Nx, Nx), occold(nblocks,Nx), occnew(nblocks,Nx), occup(nblocks,Nx))
    ! Arold and Arnew contain Nt index sets of length norder, i.e. Arold(i,:) is the old i-th such index set.
    ! occ(i,:) stores the occupation numbers for the i-th timeslice.
    allocate(Arold1(nblocks,norder1), Arnew1(nblocks,norder1), Arold2(nblocks,norder2), Arnew2(nblocks,norder2))
    ! Brold and Brnew contain Nt index sets of length norder+1, i.e. Brold(i,:) is the old i-th such index set.
    allocate(Brold1(nblocks,norder1+1), Brnew1(nblocks,norder1+1), Brold2(nblocks,norder2+1), Brnew2(nblocks,norder2+1))
    ! Woldarr and Wnewarr store all needed minor matrix elements
    allocate(Woldarr1(nblocks), Wnewarr1(nblocks), Woldarr2(nblocks), Wnewarr2(nblocks), Wuparr1(nblocks), Wuparr2(nblocks))

    ! Read gamma from invocation of program.
    call getarg(1, gammaarg)
    read(gammaarg,*) gamma

    ! Read index from invocation of program.
    call getarg(2, indexarg)
    read(indexarg,*) index
    if (index<=0 .or. index > 1000) stop "Index must be an integer in {1, 1000}."

    ! Set gh and the f_alphas
    gh = weights(index,2)
    f0 = weights(index,3)
    f1 = weights(index,4)
    f2 = weights(index,5)

    ! Create first index set (used for observable).
    do i=1,norder1
        A11(i) = i
    enddo
    do i=1, norder2
        A12(i) = i
    enddo

    ! Initialize Arold with Nt index sets {1,2,3,...,norder}
    do i=1,nblocks
        Arold1(i,:) = A11
        Arold2(i,:) = A12
    enddo

    ! Initialize occ
    do i=1,nblocks
        call OccupationNumber(Arold1(i,:),Arold2(i,:),Nx,occold(i,:))
    enddo

    ! Read filename from invocation of program.
    call getarg(3, outputname)

    ! Open file for output
    open(1, file=outputname)

    ! Runtime measurement start
    call cpu_time(start_time)

    ! Acceptance rate
    acceptance = 0

    call BuildBxx(gamma, Nx, Bxx) ! Build Bxx
    call MatPower(Bxx, Nt/nblocks, PBxx) ! Take the needed power of Bxx.

    ! Calculate the initial minor matix elements.
    do m=1,nblocks-1
        call ComplementaryMinor(Arold1(m,:), Arold1(m+1,:), PBxx, Woldarr1(m))
        call ComplementaryMinor(Arold2(m,:), Arold2(m+1,:), PBxx, Woldarr2(m))
    enddo
    call ComplementaryMinor(Arold1(nblocks,:), Arold1(1,:), PBxx, Woldarr1(nblocks)) ! Last such element.
    call ComplementaryMinor(Arold2(nblocks,:), Arold2(1,:), PBxx, Woldarr2(nblocks)) ! Last such element.
    ! Wold1 = product(Woldarr1) ! Calculate the old element
    ! Wold2 = product(Woldarr2) ! Calculate the old element
    ! Wold = Wold1*Wold2*f0**count(occold .eq. 0)*f1**count(occold .eq. 1)*f2**count(occold .eq. 2) ! This is the total weight.

    ! Initialize all new values with the old ones
    Arnew1 = Arold1
    Wnewarr1 = Woldarr1
    ! Wnew1 = Wold1
    Arnew2 = Arold2
    Wnewarr2 = Woldarr2
    ! Wnew2 = Wold2
    ! Wnew = Wold 
    occnew = occold

    ! Start in sector n
    sector = 1

    !Sector counters
    sector1 = 0
    sector2 = 0

    do k=1,Nmeasure
        do i=1,Nstat
            if (sector == 1) then
                ! Simulation in the n sector.
                do l=1,2*nblocks ! The idea here is to update fermion type 1 in the odd, and type 2 in the even steps.
                    ! Odd l
                    if (modulo(l,2)==1) then
                        call RandomMove(Arnew1((l+1)/2,:), Nx) ! Move a random fermion to the next free position in Arnew(l,:)
                        ! Since only one index set gets updated here, only two elements of Wnewarr need to be calculated
                        ! The seemingly strange modulos are needed that e.g. in the next line l = 1 uses the index sets Nt, 1.
                        call ComplementaryMinor(Arnew1(modulo((l+1)/2-2,nblocks)+1,:), Arnew1((l+1)/2,:),&
                            & PBxx, Wnewarr1(modulo((l+1)/2-2,nblocks)+1))
                        call ComplementaryMinor(Arnew1((l+1)/2,:), Arnew1(modulo((l+1)/2,nblocks)+1,:),&
                            & PBxx, Wnewarr1((l+1)/2))
                        ! Wnew1 = product(Wnewarr1) ! Calculate the new element 

                        ! Update the occupation number
                        call OccupationNumber(Arnew1((l+1)/2,:),Arnew2((l+1)/2,:),Nx,occnew((l+1)/2,:))

                        ! Calculate simweight
                        simweight = Wnewarr1(modulo((l+1)/2-2,nblocks)+1)*Wnewarr1((l+1)/2)&
                            &/(Woldarr1(modulo((l+1)/2-2,nblocks)+1)*Woldarr1((l+1)/2))&
                            &*f0**(count(occnew((l+1)/2,:) .eq. 2) - count(occold((l+1)/2,:) .eq. 2))&
                            &*f1**(count(occnew((l+1)/2,:) .eq. 1) - count(occold((l+1)/2,:) .eq. 1))&
                            &*f2**(count(occnew((l+1)/2,:) .eq. 0) - count(occold((l+1)/2,:) .eq. 0))
                    endif
                    ! Even l
                    if (modulo(l,2)==0) then
                        call RandomMove(Arnew2(l/2,:), Nx) ! Move a random fermion to the next free position in Arnew(l,:)
                        ! Since only one index set gets updated here, only two elements of Wnewarr need to be calculated
                        ! The seemingly strange modulos are needed that e.g. in the next line l = 1 uses the index sets Nt, 1.
                        call ComplementaryMinor(Arnew2(modulo(l/2-2,nblocks)+1,:), Arnew2(l/2,:),&
                            & PBxx, Wnewarr2(modulo(l/2-2,nblocks)+1))
                        call ComplementaryMinor(Arnew2(l/2,:), Arnew2(modulo(l/2,nblocks)+1,:),&
                            & PBxx, Wnewarr2(l/2))                
                        ! Wnew2 = product(Wnewarr2) ! Calculate the new element

                        ! Update the occupation number
                        call OccupationNumber(Arnew1(l/2,:),Arnew2(l/2,:),Nx,occnew(l/2,:))

                        ! Calculate simweight
                        simweight = Wnewarr2(modulo(l/2-2,nblocks)+1)*Wnewarr2(l/2)&
                            &/(Woldarr2(modulo(l/2-2,nblocks)+1)*Woldarr2(l/2))&
                            &*f0**(count(occnew(l/2,:) .eq. 2) - count(occold(l/2,:) .eq. 2))&
                            &*f1**(count(occnew(l/2,:) .eq. 1) - count(occold(l/2,:) .eq. 1))&
                            &*f2**(count(occnew(l/2,:) .eq. 0) - count(occold(l/2,:) .eq. 0))
                    endif

                    ! ! New weight
                    ! Wnew = Wnew1*Wnew2*f0**count(occnew .eq. 0)*f1**count(occnew .eq. 1)*f2**count(occnew .eq. 2)

                    u = grnd()
                    if (u < abs(simweight)) then
                        Arold1 = Arnew1
                        Woldarr1 = Wnewarr1
                        ! Wold1 = Wnew1
                        Arold2 = Arnew2
                        Woldarr2 = Wnewarr2
                        ! Wold2 = Wnew2
                        ! Wold = Wnew
                        occold = occnew
                        acceptance = acceptance + 1
                    else ! Continue from old configuration if the new one is not accepted.
                        Arnew1 = Arold1
                        Wnewarr1 = Woldarr1
                        ! Wnew1 = Wold1
                        Arnew2 = Arold2
                        Wnewarr2 = Woldarr2
                        ! Wnew2 = Wold2
                        ! Wnew = Wold
                        occnew = occold
                    endif
                enddo
                ! Here the B index sets are generated.
                ! Initialize Brold with the index sets from Arold with an added fermion.
                do m=1,nblocks
                    call AddRandomIndex(Arold1(m,:),Brold1(m,:),Nx)
                    call AddRandomIndex(Arold2(m,:),Brold2(m,:),Nx)
                enddo
                ! Initialize occup, the new occupation number in the n+1 sector
                do m=1,nblocks
                    call OccupationNumber(Brold1(m,:),Brold2(m,:),Nx,occup(m,:))
                enddo
                ! Calculate the initial minor matix elements in the n+1 sector.
                do m=1,nblocks-1
                    call ComplementaryMinor(Brold1(m,:), Brold1(m+1,:), PBxx, Wuparr1(m))
                    call ComplementaryMinor(Brold2(m,:), Brold2(m+1,:), PBxx, Wuparr2(m))
                enddo
                call ComplementaryMinor(Brold1(nblocks,:), Brold1(1,:), PBxx, Wuparr1(nblocks)) ! Last such element.
                call ComplementaryMinor(Brold2(nblocks,:), Brold2(1,:), PBxx, Wuparr2(nblocks)) ! Last such element.
                ! Weight for switching sector
                simweight = 1
                do m=1,nblocks
                    simweight = simweight*(Wuparr1(m)/Woldarr1(m))*(Wuparr2(m)/Woldarr2(m))
                enddo
                simweight = simweight&
                            &*f0**(count(occup .eq. 2) - count(occold .eq. 2))&
                            &*f1**(count(occup .eq. 1) - count(occold .eq. 1))&
                            &*f2**(count(occup .eq. 0) - count(occold .eq. 0))&
                            &*(real((norder1 + 1)*(norder2 + 1))/real((Nx-norder1)*(Nx-norder2)))**Nt
                ! Update step
                u = grnd()
                if (u < abs(simweight)) then ! Switch to the n+1 sector, i.e set sector = 0
                    sector = 0
                    Woldarr1 = Wuparr1
                    Woldarr2 = Wuparr2
                    occold = occup
                    ! Set the new index sets and occupation numbers
                    Brnew1 = Brold1
                    Wnewarr1 = Woldarr1
                    Brnew2 = Brold2
                    Wnewarr2 = Woldarr2
                    occnew = occold
                endif
                sector1 = sector1 + 1 ! This simulation step was in sector n.
            else
                ! Simulation in the n+1 sector.
                do l=1,2*nblocks ! The idea here is to update fermion type 1 in the odd, and type 2 in the even steps.
                    ! Odd l
                    if (modulo(l,2)==1) then
                        call RandomMove(Brnew1((l+1)/2,:), Nx) ! Move a random fermion to the next free position in Brnew(l,:)
                        ! Since only one index set gets updated here, only two elements of Wnewarr need to be calculated
                        ! The seemingly strange modulos are needed that e.g. in the next line l = 1 uses the index sets Nt, 1.
                        call ComplementaryMinor(Brnew1(modulo((l+1)/2-2,nblocks)+1,:), Brnew1((l+1)/2,:),&
                            & PBxx, Wnewarr1(modulo((l+1)/2-2,nblocks)+1))
                        call ComplementaryMinor(Brnew1((l+1)/2,:), Brnew1(modulo((l+1)/2,nblocks)+1,:),&
                            & PBxx, Wnewarr1((l+1)/2))
                        ! Wnew1 = product(Wnewarr1) ! Calculate the new element 

                        ! Update the occupation number
                        call OccupationNumber(Brnew1((l+1)/2,:),Brnew2((l+1)/2,:),Nx,occnew((l+1)/2,:))

                        ! Calculate simweight
                        simweight = Wnewarr1(modulo((l+1)/2-2,nblocks)+1)*Wnewarr1((l+1)/2)&
                            &/(Woldarr1(modulo((l+1)/2-2,nblocks)+1)*Woldarr1((l+1)/2))&
                            &*f0**(count(occnew((l+1)/2,:) .eq. 2) - count(occold((l+1)/2,:) .eq. 2))&
                            &*f1**(count(occnew((l+1)/2,:) .eq. 1) - count(occold((l+1)/2,:) .eq. 1))&
                            &*f2**(count(occnew((l+1)/2,:) .eq. 0) - count(occold((l+1)/2,:) .eq. 0))
                    endif
                    ! Even l
                    if (modulo(l,2)==0) then
                        call RandomMove(Brnew2(l/2,:), Nx) ! Move a random fermion to the next free position in Brnew(l,:)
                        ! Since only one index set gets updated here, only two elements of Wnewarr need to be calculated
                        ! The seemingly strange modulos are needed that e.g. in the next line l = 1 uses the index sets Nt, 1.
                        call ComplementaryMinor(Brnew2(modulo(l/2-2,nblocks)+1,:), Brnew2(l/2,:),&
                            & PBxx, Wnewarr2(modulo(l/2-2,nblocks)+1))
                        call ComplementaryMinor(Brnew2(l/2,:), Brnew2(modulo(l/2,nblocks)+1,:),&
                            & PBxx, Wnewarr2(l/2))                
                        ! Wnew2 = product(Wnewarr2) ! Calculate the new element

                        ! Update the occupation number
                        call OccupationNumber(Brnew1(l/2,:),Brnew2(l/2,:),Nx,occnew(l/2,:))

                        ! Calculate simweight
                        simweight = Wnewarr2(modulo(l/2-2,nblocks)+1)*Wnewarr2(l/2)&
                            &/(Woldarr2(modulo(l/2-2,nblocks)+1)*Woldarr2(l/2))&
                            &*f0**(count(occnew(l/2,:) .eq. 2) - count(occold(l/2,:) .eq. 2))&
                            &*f1**(count(occnew(l/2,:) .eq. 1) - count(occold(l/2,:) .eq. 1))&
                            &*f2**(count(occnew(l/2,:) .eq. 0) - count(occold(l/2,:) .eq. 0))
                    endif

                    ! ! New weight
                    ! Wnew = Wnew1*Wnew2*f0**count(occnew .eq. 0)*f1**count(occnew .eq. 1)*f2**count(occnew .eq. 2)

                    u = grnd()
                    if (u < abs(simweight)) then
                        Brold1 = Brnew1
                        Woldarr1 = Wnewarr1
                        ! Wold1 = Wnew1
                        Brold2 = Brnew2
                        Woldarr2 = Wnewarr2
                        ! Wold2 = Wnew2
                        ! Wold = Wnew
                        occold = occnew
                        acceptance = acceptance + 1
                    else ! Continue from old configuration if the new one is not accepted.
                        Brnew1 = Brold1
                        Wnewarr1 = Woldarr1
                        ! Wnew1 = Wold1
                        Brnew2 = Brold2
                        Wnewarr2 = Woldarr2
                        ! Wnew2 = Wold2
                        ! Wnew = Wold
                        occnew = occold
                    endif
                enddo
                ! Here the A index sets are generated.
                ! Initialize Arold with the index sets from Brold with one less fermions.
                do m=1,nblocks
                    call RemoveRandomIndex(Arold1(m,:),Brold1(m,:),Nx)
                    call RemoveRandomIndex(Arold2(m,:),Brold2(m,:),Nx)
                enddo
                ! Initialize occup, the new occupation number in the n+1 sector
                do m=1,nblocks
                    call OccupationNumber(Arold1(m,:),Arold2(m,:),Nx,occup(m,:))
                enddo
                ! Calculate the initial minor matix elements in the n+1 sector.
                do m=1,nblocks-1
                    call ComplementaryMinor(Arold1(m,:), Arold1(m+1,:), PBxx, Wuparr1(m))
                    call ComplementaryMinor(Arold2(m,:), Arold2(m+1,:), PBxx, Wuparr2(m))
                enddo
                call ComplementaryMinor(Arold1(nblocks,:), Arold1(1,:), PBxx, Wuparr1(nblocks)) ! Last such element.
                call ComplementaryMinor(Arold2(nblocks,:), Arold2(1,:), PBxx, Wuparr2(nblocks)) ! Last such element.
                ! Weight for switching sector
                simweight = 1
                do m=1,nblocks
                    simweight = simweight*(Wuparr1(m)/Woldarr1(m))*(Wuparr2(m)/Woldarr2(m))
                enddo
                simweight = simweight&
                            &*f0**(count(occup .eq. 2) - count(occold .eq. 2))&
                            &*f1**(count(occup .eq. 1) - count(occold .eq. 1))&
                            &*f2**(count(occup .eq. 0) - count(occold .eq. 0))&
                            &*(real((Nx-norder1)*(Nx-norder2))/real((norder1 + 1)*(norder2 + 1)))**Nt
                ! Update step
                u = grnd()
                if (u < abs(simweight)) then ! Switch to the n sector, i.e set sector = 1
                    sector = 1
                    Woldarr1 = Wuparr1
                    Woldarr2 = Wuparr2
                    occold = occup
                    ! Set the new index sets and occupation numbers
                    Arnew1 = Arold1
                    Wnewarr1 = Woldarr1
                    Arnew2 = Arold2
                    Wnewarr2 = Woldarr2
                    occnew = occold
                endif
                sector2 = sector2 + 1 ! This simulation step was in sector n+1.
            endif
        enddo
        if (modulo(k,100)==0) then ! Write data all 100 measurements for error calculation.
            write(1,*) gamma, gh, sector1, sector2
            sector1 = 0
            sector2 = 0         
        endif
    enddo
    print*, 'gh: ', gh, ', ', 'Steps: ', Nstat, '*', Nmeasure, '*', 2*nblocks, ', ', 'Accepted: ', acceptance

    close(1) ! Close output file.

    ! Runtime measurement stop
    call cpu_time(stop_time)
    print*, 'CPU-time: ', stop_time-start_time, 's'
    
end program spinpolfermions