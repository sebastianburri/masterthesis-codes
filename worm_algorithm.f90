program spinpolfermions
    ! Run ./spinpolfermions gamma index outputname seed_modifier in terminal, e.g. ./spinpolfermions 0.025 13 test.dat 221412.
    ! The index determines which gh is used: gh = index/100, it must be an integer between 1 and 1000.
    ! All other variables are set here before execution of the program.
    use subs
    implicit none
    integer :: i, j, k, l, m, n, Nx, Nt, norder1, norder2, Nstat, nblocks, seedmod, termworms, Znp1
    integer(dp) ::  nworms, Nmeasure, idp, wcount
    integer :: acceptance, index, sector, indexnext, indexbeginning, indexend, indexstart, counter, countermax ! Determines in which sector the simulation runs, 1 for sector n, 0 for n+1.
    real(dp) :: sector0, sector1, p0, p1, p2 ! sector0 counts how many simulations were in sector n, sector1 how many in sector n+1. p0, p1 and p2 control the probability to go to the "pure" sectors.
    integer, dimension(:), allocatable :: A11, A12, B11, B12, sectorcount
    integer, dimension(:,:), allocatable :: Arold1, Arnew1, Arold2, Arnew2, occold, occnew, occup ! occ will be used to store the occupation numbers.
    integer, dimension(:,:), allocatable :: Brold1, Brnew1, Brold2, Brnew2 ! Index sets used for the n+1 sector.
    integer, dimension(:,:), allocatable :: A1ini, A2ini, B1ini, B2ini, occAini, occBini ! Store initial configurations for A and B index sets.
    real(dp), dimension(:,:), allocatable :: Bxx, PBxx
    real(dp), dimension(:), allocatable :: Woldarr1, Wnewarr1, Woldarr2, Wnewarr2
    real(dp), dimension(:), allocatable :: WAarr1ini, WBarr1ini, WAarr2ini, WBarr2ini
    real(dp) :: Wold1, Wnew1, Wold2, Wnew2, Wold, Wnew, gh, gamma, simweight, simweight2 ! In simweight just the changes in weights and not the whole weight is considered.
    real(dp), dimension(1000,5) :: weights ! Used to store the f_alphas, also contains indices and gh:
                                           ! weights(index,:) = index, gh, f0, f1, f2
    character(len=32) :: indexarg, outputname, seedmodarg, gammaarg ! Used to pass arguments on execution via getarg.
    real(dp) :: f0, f1, f2, u ! These will be f_alpha, alpha = 0, 1, 2. u for double precision mersenne twister RNG.
    real :: start_time, stop_time

    ! Load the f_alphas into weights.
    open(12, file="falpha.dat")
    read(12,*) weights
    close(12)

    ! Read seed modifier from from invocation of program.
    call getarg(4, seedmodarg)
    read(seedmodarg,*) seedmod
    ! Initialise random generator
    call sgrnd(defaultsd + seedmod)

    Nx = 4
    Nt = 4
    nblocks = Nt ! Blocks for multiplication, must divide Nt without rest. Must be equal to Nt for the interacting system.
    norder1 = 2 ! Number of fermions of type 1. This scheme is used for all relevant objects.
    norder2 = 2 ! Number of fermions of type 2.
    if (modulo(Nt, nblocks) /= 0) stop "This number of blocks is not possible for the given Nt."
    Nstat = 10 ! Number of steps for one measurement
    Nmeasure = 1 ! Number of worms per measurements
    nworms = 2000*Nmeasure ! Number of worms (needs to be a multiple of Nmeasure since every Nmeasure worms a measurement is made)

    allocate(A11(norder1), A12(norder2), Bxx(Nx, Nx), PBxx(Nx, Nx), occold(nblocks,Nx), occnew(nblocks,Nx), occup(nblocks,Nx))
    allocate(B11(norder1+1), B12(norder2+1))
    ! Arold and Arnew contain Nt index sets of length norder, i.e. Arold(i,:) is the old i-th such index set.
    ! occ(i,:) stores the occupation numbers for the i-th timeslice.
    allocate(Arold1(nblocks,norder1), Arnew1(nblocks,norder1), Arold2(nblocks,norder2), Arnew2(nblocks,norder2))
    ! Brold and Brnew contain Nt index sets of length norder+1, i.e. Brold(i,:) is the old i-th such index set.
    allocate(Brold1(nblocks,norder1+1), Brnew1(nblocks,norder1+1), Brold2(nblocks,norder2+1), Brnew2(nblocks,norder2+1))
    ! Woldarr and Wnewarr store all needed minor matrix elements
    allocate(Woldarr1(nblocks), Wnewarr1(nblocks), Woldarr2(nblocks), Wnewarr2(nblocks))
    ! sectorcount stores in which sector all transfer matrices are
    allocate(sectorcount(nblocks))
    ! allocate initial variables
    allocate(A1ini(nblocks,norder1), A2ini(nblocks,norder1), B1ini(nblocks,norder1+1), B2ini(nblocks,norder1+1),&
        & occAini(nblocks,Nx), occBini(nblocks,Nx),&
        & WAarr1ini(nblocks), WBarr1ini(nblocks), WAarr2ini(nblocks), WBarr2ini(nblocks))

    ! Read gamma from invocation of program.
    call getarg(1, gammaarg)
    read(gammaarg,*) gamma

    ! Read index from invocation of program.
    call getarg(2, indexarg)
    read(indexarg,*) index
    if (index<=0 .or. index > 1000) stop "Index must be an integer in {1, 1000}."

    ! Set gh and the f_alphas
    gh = weights(index,2)
    f0 = weights(index,3)
    f1 = weights(index,4)
    f2 = weights(index,5) ! From this pont on, "index" can be overwritten.

    ! Create first index set (used for observable).
    do i=1,norder1
        A11(i) = i
    enddo
    do i=1, norder2
        A12(i) = i
    enddo
    do i=1,norder1+1
        B11(i) = i
    enddo
    do i=1, norder2+1
        B12(i) = i
    enddo

    ! Read filename from invocation of program.
    call getarg(3, outputname)

    ! Open file for output
    open(1, file=outputname)

    ! Open file for saving terminated worms
    open(2, file="termworms.dat")

    ! Runtime measurement start
    call cpu_time(start_time)

    ! Acceptance rate
    acceptance = 0

    call BuildBxx(gamma, Nx, Bxx) ! Build Bxx
    call MatPower(Bxx, Nt/nblocks, PBxx) ! Take the needed power of Bxx.

    ! Initialize Arold with Nt index sets {1,2,3,...,norder}
    do i=1,nblocks
        Arold1(i,:) = A11
        Arold2(i,:) = A12
    enddo
    A1ini = Arold1
    A2ini = Arold2
    ! Initialize Brold with Nt index sets {1,2,3,...,norder+1}
    do i=1,nblocks
        Brold1(i,:) = B11
        Brold2(i,:) = B12
    enddo
    B1ini = Brold1
    B2ini = Brold2

    ! Initialize occ
    do i=1,nblocks
        call OccupationNumber(Arold1(i,:),Arold2(i,:),Nx,occold(i,:))
    enddo
    occAini = occold
    do i=1,nblocks
        call OccupationNumber(Brold1(i,:),Brold2(i,:),Nx,occold(i,:))
    enddo
    occBini = occold

    ! Calculate the initial minor matix elements.
    do m=1,nblocks-1
        call ComplementaryMinor(Arold1(m,:), Arold1(m+1,:), PBxx, Woldarr1(m))
        call ComplementaryMinor(Arold2(m,:), Arold2(m+1,:), PBxx, Woldarr2(m))
    enddo
    call ComplementaryMinor(Arold1(nblocks,:), Arold1(1,:), PBxx, Woldarr1(nblocks)) ! Last such element.
    call ComplementaryMinor(Arold2(nblocks,:), Arold2(1,:), PBxx, Woldarr2(nblocks)) ! Last such element.
    WAarr1ini = Woldarr1
    WAarr2ini = Woldarr2
    do m=1,nblocks-1
        call ComplementaryMinor(Brold1(m,:), Brold1(m+1,:), PBxx, Woldarr1(m))
        call ComplementaryMinor(Brold2(m,:), Brold2(m+1,:), PBxx, Woldarr2(m))
    enddo
    call ComplementaryMinor(Brold1(nblocks,:), Brold1(1,:), PBxx, Woldarr1(nblocks)) ! Last such element.
    call ComplementaryMinor(Brold2(nblocks,:), Brold2(1,:), PBxx, Woldarr2(nblocks)) ! Last such element.
    WBarr1ini = Woldarr1
    WBarr2ini = Woldarr2

    ! Initialize sectorcount (in lower sector, 0 stands for sector n and 1 for sector n+1)
    do i=1,nblocks
        sectorcount(i) = 0
    enddo

    ! Thus, the start is in sector n
    Arold1 = A1ini
    Arold2 = A2ini
    occnew = occAini
    occold = occAini
    Wnewarr1 = WAarr1ini
    Woldarr1 = WAarr1ini
    Wnewarr2 = WAarr2ini
    Woldarr2 = WAarr2ini

    ! Sector counters
    sector0 = 0 ! For sector n
    sector1 = 0 ! For sector n+1

    ! counter controls how long a worm can maximally run
    counter = 0
    ! This is the maximal allowed counter
    countermax = 1000000
    ! Termworms counts the number of thus terminated worms
    termworms = 0

    ! Set p0 and p1 (they both have to be smaller than 1)
    p0 = 1.0/3.0 ! For p0 = 1/3 all possibilites have the same probability
    p1 = 1.0/3.0 ! Leave p1 = 1/3 fix, this controls the step before one reaches the Nt-1 twopointfunction
    p2 = 1.0/2.0 ! With probability p2 one goes to the partition function, and witp 1-p2 to the Nt-2 twopointfunction; i.e. for p2 = 1/2 they are both as likely as the other

    ! wcount counts how many worms finish, should be equal to nworms
    wcount = 0
    do idp=1,nworms
        ! !If counter >= countermax the worm didn't terminate normally. Put the system in it's ground state by hand.
        ! if (counter >= countermax) then
        !     termworms = termworms + 1
        !     ! Write to file (works for Nt = Nx = 4)
        !     write(2,*) "Terminated Worm No. ", termworms
        !     write(2,*) "index, indexbeginning, indexend"
        !     write(2,*) index, indexbeginning, indexend
        !     write(2,*) "Sectorcount:"
        !     write(2,*) sectorcount
        !     write(2,*) "Arold1:"
        !     write(2,*) Arold1(1,:)
        !     write(2,*) Arold1(2,:)
        !     write(2,*) Arold1(3,:)
        !     write(2,*) Arold1(4,:)
        !     write(2,*) "Arold2:"
        !     write(2,*) Arold2(1,:)
        !     write(2,*) Arold2(2,:)
        !     write(2,*) Arold2(3,:)
        !     write(2,*) Arold2(4,:)
        !     write(2,*) "Brold1:"
        !     write(2,*) Brold1(1,:)
        !     write(2,*) Brold1(2,:)
        !     write(2,*) Brold1(3,:)
        !     write(2,*) Brold1(4,:)
        !     write(2,*) "Brold2:"
        !     write(2,*) Brold2(1,:)
        !     write(2,*) Brold2(2,:)
        !     write(2,*) Brold2(3,:)
        !     write(2,*) Brold2(4,:)
        !     write(2,*) "Occupation numbers:"
        !     write(2,*) occold(1,:)
        !     write(2,*) occold(2,:)
        !     write(2,*) occold(3,:)
        !     write(2,*) occold(4,:)
        !     write(2,*) "Woldarr1:"
        !     write(2,*) Woldarr1
        !     write(2,*) "Woldarr2:"
        !     write(2,*) Woldarr2
        !     ! Initialize sectorcount (in lower sector, 0 stands for sector n and 1 for sector n+1)
        !     do j=1,nblocks
        !         sectorcount(j) = 0
        !     enddo
        !     ! Thus, the start is in sector n
        !     Arold1 = A1ini
        !     Arold2 = A2ini
        !     occnew = occAini
        !     occold = occAini
        !     Wnewarr1 = WAarr1ini
        !     Woldarr1 = WAarr1ini
        !     Wnewarr2 = WAarr2ini
        !     Woldarr2 = WAarr2ini
        !     ! To be safe, initialise Brold again as well.
        !     Brold1 = B1ini
        !     Brold2 = B2ini
        ! endif
        counter = 0
        ! Measurement
        if (modulo(idp,Nmeasure) == 0) then
            write(1,*) sector0, sector1
            sector0 = 0
            sector1 = 0
        endif
        ! Determine the starting position (start with a random timeslice)
        u = grnd()
        index = 1 + floor(nblocks*u) ! Generate random integer in {1, nblocks}.
        indexstart = index ! Keep starting index

        ! Put the timeslice at "index", i.e. "indexstart" in sector n+1 if its in sector n or in sector n if its in n+1.
        if (sectorcount(index) == 0) then ! This is enough since all slices must be in the same sector
            ! This worm starts in sector n, thus head and tail of it are on the same slice as the starting index
            indexbeginning = index ! Leftmost end of worm
            indexend = index ! Rightmost end of worm
            ! Now modify the indexsets with indices index and index+1, i.e. copy them to Brold and add a random fermion.
            call AddRandomIndex(Arold1(index,:),Brold1(index,:),Nx)
            call AddRandomIndex(Arold2(index,:),Brold2(index,:),Nx)
            call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
            call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
            ! Update weights
            call ComplementaryMinor(Brold1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
            call ComplementaryMinor(Brold2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
            ! Keep trying till the weight is not equal to zero (maybe implement a MC acceptance step here)
            do while (Wnewarr1(index) == 0)
                ! Suggest new Brold1
                call AddRandomIndex(Arold1(index,:),Brold1(index,:),Nx)
                call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                ! Update weights
                call ComplementaryMinor(Brold1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
            enddo
            do while (Wnewarr2(index) == 0)
                ! Suggest new Brold2
                call AddRandomIndex(Arold2(index,:),Brold2(index,:),Nx)
                call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                ! Update weights
                call ComplementaryMinor(Brold2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
            enddo
            ! Since the weights generated here are used as starting position, write them on Woldarr
            Woldarr1(index) = Wnewarr1(index)
            Woldarr2(index) = Wnewarr2(index)
            ! Occupation number is updated after the next step in the worm.
            ! Update sectorcount
            sectorcount(index) = 1
        else if (sectorcount(index) == 1) then
            ! This worm starts in sector n+1, thus head and tail of it are on the adjacent slices of the starting index
            indexbeginning = modulo(indexbeginning,nblocks)+1 ! Leftmost end of worm
            indexend = modulo(indexend-2,nblocks)+1! Rightmost end of worm
            ! Now modify the indexsets with indices index and index+1, i.e. copy them to Arold and remove a random fermion.
            call RemoveRandomIndex(Arold1(index,:),Brold1(index,:),Nx)
            call RemoveRandomIndex(Arold2(index,:),Brold2(index,:),Nx)
            call RemoveRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
            call RemoveRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
            ! Update weights
            call ComplementaryMinor(Arold1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
            call ComplementaryMinor(Arold2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
            ! Keep trying till the weight is not equal to zero
            do while (Wnewarr1(index) == 0)
                ! Suggest new Brold1
                call RemoveRandomIndex(Arold1(index,:),Brold1(index,:),Nx)
                call RemoveRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                ! Update weights
                call ComplementaryMinor(Arold1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
            enddo
            do while (Wnewarr2(index) == 0)
                ! Suggest new Brold2
                call RemoveRandomIndex(Arold2(index,:),Brold2(index,:),Nx)
                call RemoveRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                ! Update weights
                call ComplementaryMinor(Arold2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
            enddo
            ! Since the weights generated here are used as starting position, write them on Woldarr
            Woldarr1(index) = Wnewarr1(index)
            Woldarr2(index) = Wnewarr2(index)
            ! The "external" occupation number is updated
            call OccupationNumber(Arold1(index,:),Arold2(index,:),Nx,occnew(index,:))
            occold = occnew
            ! Update sectorcount
            sectorcount(index) = 0
        endif

        ! Update step
        do while (count(sectorcount .eq. 0) /= 0 .and. count(sectorcount .eq. 1) /= 0 .and. counter <= countermax)
            ! Determine if only one timeslice is in a different sector from the others.
            if (count(sectorcount .eq. 1) == 1) then ! Only one timeslice in sector n+1
                u = grnd()
                if (u < p0) then ! Go to where all sectors are in n
                    do k=1,nblocks
                        if (sectorcount(k) == 1) then
                            index = k
                        endif
                    enddo
                    sectorcount(index) = 0
                    if (count(sectorcount .eq. 1) /= 0) stop "Not all sectors in n."
                    ! Update weights
                    call ComplementaryMinor(Arold1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arold2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Simweight
                    simweight = (1.0/(3.0*p0))**2*Wnewarr1(index)/Woldarr1(index)*Wnewarr2(index)/Woldarr2(index) ! conisder p0 /= 1/3
                    ! Update step             
                    u = grnd()   
                    if (u < simweight) then ! The change to go to where all sectors are in n is accepted, update sector0 and start a new worm.
                        sector0 = sector0 + 1!simweight/((1.0/(3.0*p0))**2)
                        ! Now starts a new worm in sector n. Update Woldarr.
                        Woldarr1(index) = Wnewarr1(index)!*1.0/(3.0*p0)
                        Woldarr2(index) = Wnewarr2(index)!*1.0/(3.0*p0)
                        wcount = wcount + 1
                        ! sector0 = sector0 + 1
                    else ! Go back to previous configuration
                        sectorcount(index) = 1
                        counter = counter + 1
                        indexend = index
                        indexbeginning = index                        
                    endif
                else if (u > p0 .and. u < (1.0 + p0)/2.0) then ! Move indexend to the right.
                    counter = counter + 1
                    indexend = modulo(indexend,nblocks)+1
                    index = indexend ! Index used for current step is indexend
                    ! Note that only the timeslice to the left of the current "index(end)" is in sector n+1
                    ! Further, Brold(index(end),:) is already updated, i.e. only Brnew(modulo(index(end),nblocks)+1,:) needs to be updated.
                    call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brnew1(modulo(index,nblocks)+1,:),Nx)
                    call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brnew2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brold1(index,:), Brnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brold2(index,:), Brnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at index(end) - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))&
                        &*(2.0/(3.0*(1.0 - p0)))**2 ! To compensate for a p0 /= 1/3 (for both fermions).
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(end) is also in sector n+1
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)!*2.0/(3.0*(1.0 - p0))
                        Woldarr2(index) = Wnewarr2(index)!*2.0/(3.0*(1.0 - p0))
                        Brold1(modulo(index,nblocks)+1,:) = Brnew1(modulo(index,nblocks)+1,:)
                        Brold2(modulo(index,nblocks)+1,:) = Brnew2(modulo(index,nblocks)+1,:)
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the left, i.e. make new suggestion from previous position.
                        index = modulo(index-2,nblocks)+1
                        indexend = index
                    endif
                else ! Move indexbeginning to the left ((1+p0)/2 < u < 1).
                    counter = counter + 1
                    indexbeginning = modulo(indexbeginning-2,nblocks)+1
                    index = indexbeginning ! Index used for current step is indexbeginning
                    ! Note that only the timeslice to the right of the current "index(beginning)" is in sector n+1
                    ! Further, Brold(modulo(index(beginning),nblocks)+1,:) is already updated, i.e. only Brnew(index(beginning),:) needs to be updated.
                    call AddRandomIndex(Arold1(index,:),Brnew1(index,:),Nx)
                    call AddRandomIndex(Arold2(index,:),Brnew2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brnew1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brnew2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(beginning),nblocks)+1 - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))&
                        &*(2.0/(3.0*(1.0 - p0)))**2 ! To compensate for a p0 /= 1/3 (for both fermions).
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(beginning) is also in sector n+1
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)!*2.0/(3.0*(1.0 - p0))
                        Woldarr2(index) = Wnewarr2(index)!*2.0/(3.0*(1.0 - p0))
                        Brold1(index,:) = Brnew1(index,:)
                        Brold2(index,:) = Brnew2(index,:)
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the right, i.e. make new suggestion from previous position.
                        index = modulo(index,nblocks)+1
                        indexbeginning = index
                    endif
                endif

            else if (count(sectorcount .eq. 0) == 1) then ! Only one slice in sector n
                u = grnd()
                if (u < p1) then ! Go to where all sectors are in n+1. This gives in a first step a contribution to the twopoint function over the whole lattice,
                                 ! then in a second step to Z(n+1).
                    ! Move either indexbeginning to the left, or indexend to the right (matters for occupation number)
                    u = grnd()
                    if (u < 1.0/2.0) then ! Move indexbeginning to the left
                        ! indexbeginning = modulo(indexbeginning-2,nblocks)+1
                        ! index = indexbeginning ! Index used for current step is indexbeginning
                        do k=1,nblocks
                            if (sectorcount(k) == 0) then
                                index = k
                            endif
                        enddo
                        indexbeginning = index
                        sectorcount(index) = 1
                        if (count(sectorcount .eq. 0) /= 0) stop "Not all sectors in n+1."
                        ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(beginning),nblocks)+1 - which
                        ! is shared with the next timeslice)
                        call OccupationNumber(Brold1(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),&
                            &Nx,occnew(modulo(index,nblocks)+1,:))
                        ! Update weights
                        call ComplementaryMinor(Brold1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                        call ComplementaryMinor(Brold2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                        ! Simweight
                        simweight = (1.0/(3.0*p1))**2*Wnewarr1(index)/Woldarr1(index)*Wnewarr2(index)/Woldarr2(index)& ! conisder p1 /= 1/3.
                            &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - &
                                &count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                            &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - &
                                &count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                            &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - &
                                &count(occold(modulo(index,nblocks)+1,:) .eq. 2))                        
                        u = grnd()
                        if (u < simweight) then ! Now everything is in n+1, try to go to Z(n+1)
                            ! Znp1 controls if one is in the Nt-1 twopointfunction case or in the partition function case.
                            Znp1 = 0
                            do while (Znp1 == 0)
                                u = grnd()
                                if (u < p2) then ! Actually try to go to Z(n+1)
                                    ! Update last occupation number
                                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                                    ! Simweight
                                    simweight2 = (1.0/(2.0*p2))**2*f0**(count(occnew(index,:) .eq. 0) - &
                                            &count(occold(index,:) .eq. 0))&
                                        &*f1**(count(occnew(index,:) .eq. 1) - &
                                            &count(occold(index,:) .eq. 1))&
                                        &*f2**(count(occnew(index,:) .eq. 2) - &
                                            &count(occold(index,:) .eq. 2))                                
                                    ! Update step
                                    u = grnd()
                                    if (u < simweight2) then ! Update sector1, start new worm
                                        Znp1 = 1
                                        sector1 = sector1 + 1!simweight2/((1.0/(2.0*p2))**2)
                                        ! Now starts a new worm in sector n+1. Update Woldarr and occold.
                                        Woldarr1(index) = Wnewarr1(index)!*1.0/(3.0*p1)
                                        Woldarr2(index) = Wnewarr2(index)!*1.0/(3.0*p1)
                                        occold = occnew
                                        wcount = wcount + 1
                                        ! sector1 = sector1 + 1
                                    else ! Stay in the Nt-1 twopointfunction sector
                                        counter = counter + 1
                                    endif
                                else ! Try to go back to Nt-2 twopointfunction sector (with new indexsets)
                                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                                    ! Update weights
                                    call ComplementaryMinor(Arnew1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, &
                                        &Wnewarr1(index))
                                    call ComplementaryMinor(Arnew2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, &
                                        &Wnewarr2(index))
                                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at modulo(index,nblocks)+1)
                                    call OccupationNumber(Arnew1(modulo(index,nblocks)+1,:),Arnew2(modulo(index,nblocks)+1,:),&
                                        &Nx,occnew(modulo(index,nblocks)+1,:))
                                    ! Simweight
                                    simweight2=1.0/real(norder1+1)**2*Wnewarr1(index)/Woldarr1(index)&
                                        &*1/real(norder2+1)**2*Wnewarr2(index)/Woldarr2(index)&
                                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - &
                                            &count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - &
                                            &count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - &
                                            &count(occold(modulo(index,nblocks)+1,:) .eq. 2))&
                                        &*(1.0/(2.0*(1.0-p2)))**2 ! To compensate for a p2 /= 1/2 (for both fermions).
                                    ! Update step
                                    u = grnd()
                                    if (u < simweight2) then ! go back to Nt-2 twopointfunction sector
                                        Znp1 = 1
                                        sectorcount(index) = 0
                                        counter = counter + 1
                                        ! Save new indexsets, weights and occupation number
                                        Arold1(index,:) = Arnew1(index,:)
                                        Arold2(index,:) = Arnew2(index,:)
                                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                                        Woldarr1(index) = Wnewarr1(index)
                                        Woldarr2(index) = Wnewarr2(index)
                                        occold = occnew
                                        ! Move indexbeginning back to the right
                                        indexbeginning = modulo(indexbeginning,nblocks)+1
                                        index = indexbeginning                                    
                                    else ! Stay in the Nt-1 twopointfunction sector
                                        counter = counter + 1
                                    endif
                                endif      
                            enddo                                                        
                        else ! Go back to where 1 sector is still in n
                            sectorcount(index) = 0
                            counter = counter + 1
                            ! Move indexbeginning back to the right
                            indexbeginning = modulo(indexbeginning,nblocks)+1
                            index = indexbeginning
                        endif                   
                    else ! Move indexend to the right
                        ! indexend = modulo(indexend,nblocks)+1
                        ! index = indexend ! Index used for current step is indexend
                        do k=1,nblocks
                            if (sectorcount(k) == 0) then
                                index = k
                            endif
                        enddo
                        indexend = index
                        sectorcount(index) = 1
                        if (count(sectorcount .eq. 0) /= 0) stop "Not all sectors in n+1."
                        ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(end)-2,nblocks)+1 - which
                        ! is shared with the previous timeslice)
                        call OccupationNumber(Brold1(modulo(index-2,nblocks)+1,:),Brold2(modulo(index-2,nblocks)+1,:),&
                            &Nx,occnew(modulo(index-2,nblocks)+1,:))
                        ! Update weights
                        call ComplementaryMinor(Brold1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                        call ComplementaryMinor(Brold2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                        simweight = (1.0/(3.0*p1))**2*Wnewarr1(index)/Woldarr1(index)*Wnewarr2(index)/Woldarr2(index)& ! conisder p1 /= 1/3.
                            &*f0**(count(occnew(modulo(index-2,nblocks)+1,:) .eq. 0) - &
                                &count(occold(modulo(index-2,nblocks)+1,:) .eq. 0))&
                            &*f1**(count(occnew(modulo(index-2,nblocks)+1,:) .eq. 1) - &
                                &count(occold(modulo(index-2,nblocks)+1,:) .eq. 1))&
                            &*f2**(count(occnew(modulo(index-2,nblocks)+1,:) .eq. 2) - &
                                &count(occold(modulo(index-2,nblocks)+1,:) .eq. 2))
                        u = grnd()
                        if (u < simweight) then ! Now everything is in n+1, try to go to Z(n+1)
                            ! Znp1 controls if one is in the Nt-1 twopointfunction case or in the partition function case.
                            Znp1 = 0
                            do while (Znp1 == 0)
                                u = grnd()
                                if (u < p2) then ! Actually try to go to Z(n+1)
                                    ! Update last occupation number
                                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                                    ! Simweight
                                    simweight2 = (1.0/(2.0*p2))**2*f0**(count(occnew(index,:) .eq. 0) - &
                                            &count(occold(index,:) .eq. 0))&
                                        &*f1**(count(occnew(index,:) .eq. 1) - &
                                            &count(occold(index,:) .eq. 1))&
                                        &*f2**(count(occnew(index,:) .eq. 2) - &
                                            &count(occold(index,:) .eq. 2))                                
                                    ! Update step
                                    u = grnd()
                                    if (u < simweight2) then ! Update sector1, start new worm
                                        Znp1 = 1
                                        sector1 = sector1 + 1!simweight2/((1.0/(2.0*p2))**2)
                                        ! Now starts a new worm in sector n+1. Update Woldarr and occold.
                                        Woldarr1(index) = Wnewarr1(index)!*1.0/(3.0*p1)
                                        Woldarr2(index) = Wnewarr2(index)!*1.0/(3.0*p1)
                                        occold = occnew
                                        wcount = wcount + 1
                                        ! sector1 = sector1 + 1
                                    else ! Stay in the Nt-1 twopointfunction sector
                                        counter = counter + 1
                                    endif
                                else ! Try to go back to Nt-2 twopointfunction sector (with new indexsets)
                                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                                    ! Update weights
                                    call ComplementaryMinor(Arnew1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, &
                                        &Wnewarr1(index))
                                    call ComplementaryMinor(Arnew2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, &
                                        &Wnewarr2(index))
                                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at index
                                    call OccupationNumber(Arnew1(index,:),Arnew2(index,:),&
                                        &Nx,occnew(index,:))
                                    ! Simweight
                                    simweight2=1.0/real(norder1+1)**2*Wnewarr1(index)/Woldarr1(index)&
                                        &*1/real(norder2+1)**2*Wnewarr2(index)/Woldarr2(index)&
                                        &*f0**(count(occnew(index,:) .eq. 0) - &
                                            &count(occold(index,:) .eq. 0))&
                                        &*f1**(count(occnew(index,:) .eq. 1) - &
                                            &count(occold(index,:) .eq. 1))&
                                        &*f2**(count(occnew(index,:) .eq. 2) - &
                                            &count(occold(index,:) .eq. 2))&
                                        &*(1.0/(2.0*(1.0-p2)))**2 ! To compensate for a p2 /= 1/2 (for both fermions).
                                    ! Update step
                                    u = grnd()
                                    if (u < simweight2) then ! go back to Nt-2 twopointfunction sector
                                        Znp1 = 1
                                        sectorcount(index) = 0
                                        counter = counter + 1
                                        ! Save new indexsets, weights and occupation number
                                        Arold1(index,:) = Arnew1(index,:)
                                        Arold2(index,:) = Arnew2(index,:)
                                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                                        Woldarr1(index) = Wnewarr1(index)
                                        Woldarr2(index) = Wnewarr2(index)
                                        occold = occnew
                                        ! Move indexend back to the left
                                        indexend = modulo(indexend-2,nblocks)+1
                                        index = indexend                                   
                                    else ! Stay in the Nt-1 twopointfunction sector
                                        counter = counter + 1
                                    endif
                                endif      
                            enddo                                                        
                        else ! Go back to where 1 sector is still in n
                            sectorcount(index) = 0
                            counter = counter + 1
                            ! Move indexend back to the left
                            indexend = modulo(indexend-2,nblocks)+1
                            index = indexend
                        endif
                    endif
                else if (u > p1 .and. u < (1.0 + p1)/2.0) then ! Move to the right, i.e. move indexbeginning to the right.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexbeginning
                    ! Note that only the timeslice to the left of the current "index(beginning)" is in sector n
                    ! Further, Arold(index,:) is already updated, i.e. only Arnew(modulo(index,nblocks)+1,:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arold1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arold2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at modulo(index,nblocks)+1)
                    call OccupationNumber(Arnew1(modulo(index,nblocks)+1,:),Arnew2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))&
                        &*(2.0/(3.0*(1.0 - p1)))**2 ! To compensate for a p1 /= 1/3 (for both fermions).
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(beginning) is also in sector n
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)!*2.0/(3.0*(1.0 - p1))
                        Woldarr2(index) = Wnewarr2(index)!*2.0/(3.0*(1.0 - p1))
                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                        indexbeginning = modulo(indexbeginning,nblocks)+1
                        index = indexbeginning
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexbeginning is not moved in this scenario.
                    endif
                else ! Move to the left, i.e.move indexend to the left ((1+p1)/2 < u < 1).
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexend ! Index used for current step is indexend
                    ! Note that only the timeslice to the right of the current "index(end)" is in sector n
                    ! Further, Arold(modulo(index(end),nblocks)+1,:) is already updated, i.e. only Arnew(index(end),:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arnew1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arnew2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at index)
                    call OccupationNumber(Arnew1(index,:),Arnew2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))&
                        &*(2.0/(3.0*(1.0 - p1)))**2 ! To compensate for a p1 /= 1/3 (for both fermions).
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(end) is also in sector n
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)!*2.0/(3.0*(1.0 - p1))
                        Woldarr2(index) = Wnewarr2(index)!*2.0/(3.0*(1.0 - p1))
                        Arold1(index,:) = Arnew1(index,:)
                        Arold2(index,:) = Arnew2(index,:)
                        indexend = modulo(indexend-2,nblocks)+1
                        index = indexend
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexend is not moved in this scenario
                    endif
                endif
            else if (count(sectorcount .eq. 0) == 2) then ! Consider changes to weight due to p0.
                u = grnd()
                if (u < 1.0/4.0) then ! Move indexend to the right.
                    counter = counter + 1
                    indexend = modulo(indexend,nblocks)+1
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the left of the current "index(end)" is in sector n+1
                    ! Further, Brold(index(end),:) is already updated, i.e. only Brnew(modulo(index(end),nblocks)+1,:) needs to be updated.
                    call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brnew1(modulo(index,nblocks)+1,:),Nx)
                    call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brnew2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brold1(index,:), Brnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brold2(index,:), Brnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at index(end) - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(end) is also in sector n+1
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Brold1(modulo(index,nblocks)+1,:) = Brnew1(modulo(index,nblocks)+1,:)
                        Brold2(modulo(index,nblocks)+1,:) = Brnew2(modulo(index,nblocks)+1,:)
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the left, i.e. make new suggestion from previous position.
                        index = modulo(index-2,nblocks)+1
                        indexend = index
                    endif
                else if (u < 2.0/4.0 .and. u > 1.0/4.0) then ! Move indexend to the left.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the right of the current "index(end)" is in sector n
                    ! Further, Arold(modulo(index(end),nblocks)+1,:) is already updated, i.e. only Arnew(index(end),:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arnew1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arnew2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at index)
                    call OccupationNumber(Arnew1(index,:),Arnew2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))&
                        &*(1.0/(1.0 - p0))**2
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(end) is also in sector n
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)!*1.0/(1.0 - p0)
                        Woldarr2(index) = Wnewarr2(index)!*1.0/(1.0 - p0)
                        Arold1(index,:) = Arnew1(index,:)
                        Arold2(index,:) = Arnew2(index,:)
                        indexend = modulo(indexend-2,nblocks)+1
                        index = indexend
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexend is not moved in this scenario
                    endif
                else if (u < 3.0/4.0 .and. u > 2.0/4.0) then ! Move indexbeginning to the right.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexbeginning
                    ! Note that the timeslice to the left of the current "index(beginning)" is in sector n
                    ! Further, Arold(index,:) is already updated, i.e. only Arnew(modulo(index,nblocks)+1,:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arold1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arold2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at modulo(index,nblocks)+1)
                    call OccupationNumber(Arnew1(modulo(index,nblocks)+1,:),Arnew2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))&
                        &*(1.0/(1.0 - p0))**2
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(beginning) is also in sector n
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)!*1.0/(1.0 - p0)
                        Woldarr2(index) = Wnewarr2(index)!*1.0/(1.0 - p0)
                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                        indexbeginning = modulo(indexbeginning,nblocks)+1
                        index = indexbeginning
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexbeginning is not moved in this scenario.
                    endif
                else ! Move indexbeginning to the left.
                    counter = counter + 1
                    indexbeginning = modulo(indexbeginning-2,nblocks)+1
                    index = indexbeginning ! Index used for current step is indexbeginning
                    ! Note that the timeslice to the right of the current "index(beginning)" is in sector n+1
                    ! Further, Brold(modulo(index(beginning),nblocks)+1,:) is already updated, i.e. only Brnew(index(beginning),:) needs to be updated.
                    call AddRandomIndex(Arold1(index,:),Brnew1(index,:),Nx)
                    call AddRandomIndex(Arold2(index,:),Brnew2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brnew1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brnew2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(beginning),nblocks)+1 - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(beginning) is also in sector n+1
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Brold1(index,:) = Brnew1(index,:)
                        Brold2(index,:) = Brnew2(index,:)
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the right, i.e. make new suggestion from previous position.
                        index = modulo(index,nblocks)+1
                        indexbeginning = index
                    endif
                endif
            else if (count(sectorcount .eq. 1) == 2) then ! Consider changes to weight due to p1.
                u = grnd()
                if (u < 1.0/4.0) then ! Move indexend to the right.
                    counter = counter + 1
                    indexend = modulo(indexend,nblocks)+1
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the left of the current "index(end)" is in sector n+1
                    ! Further, Brold(index(end),:) is already updated, i.e. only Brnew(modulo(index(end),nblocks)+1,:) needs to be updated.
                    call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brnew1(modulo(index,nblocks)+1,:),Nx)
                    call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brnew2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brold1(index,:), Brnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brold2(index,:), Brnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at index(end) - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))&
                        &*(1.0/(1.0 - p1))**2
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(end) is also in sector n+1
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)!*1.0/(1.0 - p1)
                        Woldarr2(index) = Wnewarr2(index)!*1.0/(1.0 - p1)
                        Brold1(modulo(index,nblocks)+1,:) = Brnew1(modulo(index,nblocks)+1,:)
                        Brold2(modulo(index,nblocks)+1,:) = Brnew2(modulo(index,nblocks)+1,:)
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the left, i.e. make new suggestion from previous position.
                        index = modulo(index-2,nblocks)+1
                        indexend = index
                    endif
                else if (u < 2.0/4.0 .and. u > 1.0/4.0) then ! Move indexend to the left.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the right of the current "index(end)" is in sector n
                    ! Further, Arold(modulo(index(end),nblocks)+1,:) is already updated, i.e. only Arnew(index(end),:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arnew1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arnew2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at index)
                    call OccupationNumber(Arnew1(index,:),Arnew2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(end) is also in sector n
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Arold1(index,:) = Arnew1(index,:)
                        Arold2(index,:) = Arnew2(index,:)
                        indexend = modulo(indexend-2,nblocks)+1
                        index = indexend
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexend is not moved in this scenario
                    endif
                else if (u < 3.0/4.0 .and. u > 2.0/4.0) then ! Move indexbeginning to the right.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexbeginning
                    ! Note that the timeslice to the left of the current "index(beginning)" is in sector n
                    ! Further, Arold(index,:) is already updated, i.e. only Arnew(modulo(index,nblocks)+1,:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arold1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arold2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at modulo(index,nblocks)+1)
                    call OccupationNumber(Arnew1(modulo(index,nblocks)+1,:),Arnew2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(beginning) is also in sector n
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                        indexbeginning = modulo(indexbeginning,nblocks)+1
                        index = indexbeginning
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexbeginning is not moved in this scenario.
                    endif
                else ! Move indexbeginning to the left.
                    counter = counter + 1
                    indexbeginning = modulo(indexbeginning-2,nblocks)+1
                    index = indexbeginning ! Index used for current step is indexbeginning
                    ! Note that the timeslice to the right of the current "index(beginning)" is in sector n+1
                    ! Further, Brold(modulo(index(beginning),nblocks)+1,:) is already updated, i.e. only Brnew(index(beginning),:) needs to be updated.
                    call AddRandomIndex(Arold1(index,:),Brnew1(index,:),Nx)
                    call AddRandomIndex(Arold2(index,:),Brnew2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brnew1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brnew2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(beginning),nblocks)+1 - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))&
                        &*(1.0/(1.0 - p1))**2
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(beginning) is also in sector n+1
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)!*1.0/(1.0 - p1)
                        Woldarr2(index) = Wnewarr2(index)!*1.0/(1.0 - p1)
                        Brold1(index,:) = Brnew1(index,:)
                        Brold2(index,:) = Brnew2(index,:)
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the right, i.e. make new suggestion from previous position.
                        index = modulo(index,nblocks)+1
                        indexbeginning = index
                    endif
                endif
            else ! Normal sector
                u = grnd()
                if (u < 1.0/4.0) then ! Move indexend to the right.
                    counter = counter + 1
                    indexend = modulo(indexend,nblocks)+1
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the left of the current "index(end)" is in sector n+1
                    ! Further, Brold(index(end),:) is already updated, i.e. only Brnew(modulo(index(end),nblocks)+1,:) needs to be updated.
                    call AddRandomIndex(Arold1(modulo(index,nblocks)+1,:),Brnew1(modulo(index,nblocks)+1,:),Nx)
                    call AddRandomIndex(Arold2(modulo(index,nblocks)+1,:),Brnew2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brold1(index,:), Brnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brold2(index,:), Brnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at index(end) - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(index,:),Brold2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(end) is also in sector n+1
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Brold1(modulo(index,nblocks)+1,:) = Brnew1(modulo(index,nblocks)+1,:)
                        Brold2(modulo(index,nblocks)+1,:) = Brnew2(modulo(index,nblocks)+1,:)
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the left, i.e. make new suggestion from previous position.
                        index = modulo(index-2,nblocks)+1
                        indexend = index
                    endif
                else if (u < 2.0/4.0 .and. u > 1.0/4.0) then ! Move indexend to the left.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexend ! Index used for current step is indexend
                    ! Note that the timeslice to the right of the current "index(end)" is in sector n
                    ! Further, Arold(modulo(index(end),nblocks)+1,:) is already updated, i.e. only Arnew(index(end),:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(index,:),Brold1(index,:),Nx)
                    call RemoveRandomIndex(Arnew2(index,:),Brold2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arnew1(index,:), Arold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arnew2(index,:), Arold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at index)
                    call OccupationNumber(Arnew1(index,:),Arnew2(index,:),Nx,occnew(index,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(index,:) .eq. 0) - count(occold(index,:) .eq. 0))&
                        &*f1**(count(occnew(index,:) .eq. 1) - count(occold(index,:) .eq. 1))&
                        &*f2**(count(occnew(index,:) .eq. 2) - count(occold(index,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(end) is also in sector n
                        occold(index,:) = occnew(index,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Arold1(index,:) = Arnew1(index,:)
                        Arold2(index,:) = Arnew2(index,:)
                        indexend = modulo(indexend-2,nblocks)+1
                        index = indexend
                    else
                        occnew(index,:) = occold(index,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexend is not moved in this scenario
                    endif
                else if (u < 3.0/4.0 .and. u > 2.0/4.0) then ! Move indexbeginning to the right.
                    counter = counter + 1
                    ! Indexbeginning will be moved in the updatestep, it is more convenient this way since a fermion gets removed.
                    index = indexbeginning
                    ! Note that the timeslice to the left of the current "index(beginning)" is in sector n
                    ! Further, Arold(index,:) is already updated, i.e. only Arnew(modulo(index,nblocks)+1,:) needs to be updated.
                    call RemoveRandomIndex(Arnew1(modulo(index,nblocks)+1,:),Brold1(modulo(index,nblocks)+1,:),Nx)
                    call RemoveRandomIndex(Arnew2(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Arold1(index,:), Arnew1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Arold2(index,:), Arnew2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the new "external" occupation number, i.e. the one at modulo(index,nblocks)+1)
                    call OccupationNumber(Arnew1(modulo(index,nblocks)+1,:),Arnew2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(Nx-norder1)/real(norder1+1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(Nx-norder2)/real(norder2+1)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 0 ! Now the timeslice at index(beginning) is also in sector n
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Arold1(modulo(index,nblocks)+1,:) = Arnew1(modulo(index,nblocks)+1,:)
                        Arold2(modulo(index,nblocks)+1,:) = Arnew2(modulo(index,nblocks)+1,:)
                        indexbeginning = modulo(indexbeginning,nblocks)+1
                        index = indexbeginning
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! indexbeginning is not moved in this scenario.
                    endif
                else ! Move indexbeginning to the left.
                    counter = counter + 1
                    indexbeginning = modulo(indexbeginning-2,nblocks)+1
                    index = indexbeginning ! Index used for current step is indexbeginning
                    ! Note that the timeslice to the right of the current "index(beginning)" is in sector n+1
                    ! Further, Brold(modulo(index(beginning),nblocks)+1,:) is already updated, i.e. only Brnew(index(beginning),:) needs to be updated.
                    call AddRandomIndex(Arold1(index,:),Brnew1(index,:),Nx)
                    call AddRandomIndex(Arold2(index,:),Brnew2(index,:),Nx)
                    ! Update weights
                    call ComplementaryMinor(Brnew1(index,:), Brold1(modulo(index,nblocks)+1,:), PBxx, Wnewarr1(index))
                    call ComplementaryMinor(Brnew2(index,:), Brold2(modulo(index,nblocks)+1,:), PBxx, Wnewarr2(index))
                    ! Update the occupation number (only the "internal" occupation number, i.e. the one at modulo(index(beginning),nblocks)+1 - which
                    ! is shared with the previous timeslice)
                    call OccupationNumber(Brold1(modulo(index,nblocks)+1,:),Brold2(modulo(index,nblocks)+1,:),&
                        &Nx,occnew(modulo(index,nblocks)+1,:))
                    ! Simweight
                    simweight=real(norder1+1)/real(Nx-norder1)*Wnewarr1(index)/Woldarr1(index)&
                        &*real(norder2+1)/real(Nx-norder2)*Wnewarr2(index)/Woldarr2(index)&
                        &*f0**(count(occnew(modulo(index,nblocks)+1,:) .eq. 0) - count(occold(modulo(index,nblocks)+1,:) .eq. 0))&
                        &*f1**(count(occnew(modulo(index,nblocks)+1,:) .eq. 1) - count(occold(modulo(index,nblocks)+1,:) .eq. 1))&
                        &*f2**(count(occnew(modulo(index,nblocks)+1,:) .eq. 2) - count(occold(modulo(index,nblocks)+1,:) .eq. 2))
                    ! Update step
                    u = grnd()
                    if (u < simweight) then
                        sectorcount(index) = 1 ! Now the timeslice at index(beginning) is also in sector n+1
                        occold(modulo(index,nblocks)+1,:) = occnew(modulo(index,nblocks)+1,:)
                        Woldarr1(index) = Wnewarr1(index)
                        Woldarr2(index) = Wnewarr2(index)
                        Brold1(index,:) = Brnew1(index,:)
                        Brold2(index,:) = Brnew2(index,:)
                    else
                        occnew(modulo(index,nblocks)+1,:) = occold(modulo(index,nblocks)+1,:)
                        Wnewarr1(index) = Woldarr1(index)
                        Wnewarr2(index) = Woldarr2(index)
                        ! Move index to the right, i.e. make new suggestion from previous position.
                        index = modulo(index,nblocks)+1
                        indexbeginning = index
                    endif
                endif
            endif
        enddo
    enddo

    ! print*, 'gh: ', gh, ', ', 'Steps: ', Nstat, '*', Nmeasure, '*', 2*nblocks, ', ', 'Accepted: ', acceptance
    ! print*, sector0, sector1, sector1/sector0

    close(1) ! Close output file.

    close(2) ! Close file for terminated worms

    ! Runtime measurement stop
    call cpu_time(stop_time)
    print*, termworms, "worm(s) terminated due to reaching maximum length."
    print*, wcount, "worms terminated in total."
    print*, 'CPU-time: ', stop_time-start_time, 's'
    
end program spinpolfermions